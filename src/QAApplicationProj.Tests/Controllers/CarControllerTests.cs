﻿using Moq;
using QAApplicationProj.Controllers;
using QAApplicationProj.Data.Entities;
using QAApplicationProj.Data.Repositories;
using QAApplicationProj.Data.Repositories.Interfaces;
using QAApplicationProj.Requests;
using QAApplicationProj.Responses;
using Xunit;

namespace QAApplicationProj.Tests.Controllers
{
    public class CarControllerTests {
        private CarController _controller;
        private Mock<ICarRepository> mock;

        public CarControllerTests() {
            /* TODO; Hint: figure out a solution to avoid communicating with the database, then change the null reference to an instance of ICarRepository. 
             * You can choose to use a 3rd party library in NuGet if you'd like. */
            mock = new Mock<ICarRepository>();
            _controller = new CarController(mock.Object);
        }

        [Fact]
        public void CreateCar_Fail_InvalidMake() {
            CarRequest carRequest = new CarRequest();
            carRequest.Make = null;
            carRequest.Model = "FocusST";
            carRequest.Year = 2015;

            CreateCarResponse response = _controller.CreateCar(carRequest);

            Assert.False(response.WasSuccessful);
            Assert.Equal("The request did not pass validation.", response.Message);
        }

        [Fact]
        public void CreateCar_Fail_InvalidModel()
        {
            CarRequest carRequest = new CarRequest();
            carRequest.Make = "Ford";
            carRequest.Model = string.Empty;
            carRequest.Year = 2015;

            CreateCarResponse response = _controller.CreateCar(carRequest);

            Assert.False(response.WasSuccessful);
            Assert.Equal("The request did not pass validation.", response.Message);
        }

        [Fact]
        public void CreateCar_Fail_InvalidYear()
        {
            CarRequest carRequest = new CarRequest();
            carRequest.Make = "Ford";
            carRequest.Model = "FocusST";
            carRequest.Year = 0;

            CreateCarResponse response = _controller.CreateCar(carRequest);

            Assert.False(response.WasSuccessful);
            Assert.Equal("The request did not pass validation.", response.Message);
        }

        [Fact]
        public void CreateCar_Fail_CarExists()
        {
            CarRequest carRequest = new CarRequest();
            carRequest.Make = "Ford";
            carRequest.Model = "FocusST";
            carRequest.Year = 2015;

            // Exists returns true
            mock.Setup(carRepository => carRepository.Exists(It.IsAny<Car>())).Returns(true);
            var modifiedController = new CarController(mock.Object);
            _controller = modifiedController;

            CreateCarResponse response = _controller.CreateCar(carRequest);

            Assert.False(response.WasSuccessful);
            Assert.Equal("The requested Car already exists.", response.Message);
        }

        [Fact]
        public void CreateCar_Pass_NewCar()
        {
            CarRequest carRequest = new CarRequest();
            carRequest.Make = "Ford";
            carRequest.Model = "FocusST";
            carRequest.Year = 2015;

            // Exists returns false
            mock.Setup(carRepository => carRepository.Exists(It.IsAny<Car>())).Returns(false);
            var modifiedController = new CarController(mock.Object);
            _controller = modifiedController;

            CreateCarResponse response = _controller.CreateCar(carRequest);

            Assert.True(response.WasSuccessful);
            Assert.Equal(null, response.Message);
        }
    }
}
