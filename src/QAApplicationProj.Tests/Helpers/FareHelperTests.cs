﻿using System;

using QAApplicationProj.Helpers;
using Xunit;

namespace QAApplicationProj.Tests.Helpers
{
    public class FareHelperTests
    {
        [Fact]
        public void AddFares_RangeException_NegativeFirstFare()
        {
            decimal firstFare = -1;
            decimal secondFare = 1;

            Exception ex = Assert.Throws<ArgumentOutOfRangeException>(() => FareHelper.AddFares(firstFare, secondFare));
            Assert.Equal("The first fare must be positive.\r\nParameter name: firstFare", ex.Message);
        }

        [Fact]
        public void AddFares_RangeException_NegativeSecondFare()
        {
            decimal firstFare = 1;
            decimal secondFare = -1;

            Exception ex = Assert.Throws<ArgumentOutOfRangeException>(() => FareHelper.AddFares(firstFare, secondFare));
            Assert.Equal("The second fare must be positive.\r\nParameter name: secondFare", ex.Message);
        }

        [Fact]
        public void AddFares_OverflowException_LargeFares()
        {
            decimal firstFare = decimal.MaxValue;
            decimal secondFare = 1m;

            Assert.Throws<OverflowException>(() => FareHelper.AddFares(firstFare, secondFare));
        }

        [Fact]
        public void AddFares_Pass_MaxFare()
        {
            decimal firstFare = 1.00m;
            decimal secondFare = 79228162514264337593543950334.00m;

            Assert.Equal(decimal.MaxValue, FareHelper.AddFares(firstFare, secondFare), 2);
        }

        [Fact]
        public void AddFares_Pass_ZeroFirstFare()
        {
            decimal firstFare = 0;
            decimal secondFare = 1;

            Assert.Equal(1, FareHelper.AddFares(firstFare, secondFare), 2);
        }

        [Fact]
        public void AddFares_Pass_ZeroSecondFare()
        {
            decimal firstFare = 1;
            decimal secondFare = 0;

            Assert.Equal(1, FareHelper.AddFares(firstFare, secondFare), 2);
        }

        [Fact]
        public void AddFares_Pass_PositiveDecimalFares()
        {
            decimal firstFare = 10.02m;
            decimal secondFare = 4.70m;

            Assert.Equal(14.72m, FareHelper.AddFares(firstFare, secondFare), 2);
        }

        [Fact]
        public void AddFares_Pass_RoundedDecimalFares()
        {
            decimal firstFare = 9.999999999m;
            decimal secondFare = 5.49556m;

            Assert.Equal(15.50m, FareHelper.AddFares(firstFare, secondFare), 2);
        }
    }
}
